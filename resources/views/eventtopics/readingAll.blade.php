<aside>
    <table>
        <thead>
            <tr>
                <td>select</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($eventtopicsList as $eventtopic) 
            <tr>
                <td><a href="{{ route('eventtopics.show',$eventtopic->id)}}">show</a></td>
                <td>{{$eventtopic->name}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</aside>