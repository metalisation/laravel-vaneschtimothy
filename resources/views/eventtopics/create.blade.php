<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="Timothy Van Esch">
        <link rel="stylesheet" href="{{ asset('css/fricfrac.css') }}" type="text/css"/>
        <title>Fric-Frac</title>
    </head>
    <body>
        <main>
            <article>
                <header>
                    <h2>EventTopic</h2>
                    <nav>
                        <button type="submit" value="insert" form="form">Insert</button>
                        <a href="{{route('eventtopics.index')}}">Annuleren</a>
                    </nav>
                </header>
                <form method="post" action="{{ route('eventtopics.store') }}">
                    @csrf
   				<div>
   					<label for="Name">Naam</label>
   					<input type="text" id="Name" name="name">
   				</div>
                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
            </article>
            @include('eventtopics.readingall')
        </main>
</body>
</html>