<?php //edit = updateone ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="Timothy Van Esch">
        <link rel="stylesheet" href="{{ asset('css/fricfrac.css') }}" type="text/css"/>
        <title>Fric-Frac</title>
     </head>
  <body>
    <main>
      <article>
        <header>
    			<h2>EventTopic</h2>
    			<nav>
    				<a href="{{route('eventtopics.edit', ['id' => $eventtopic->id])}}">Edit</a>
    				<a href="{{route('eventtopics.create')}}">Create</a>
    				<button type="submit" value="delete" form="form">Delete</button>
    				<a href="{{route('eventtopics.index')}}">Annuleren</a>
    			</nav>
    		</header>
        <form action="{{ route('eventtopics.destroy', ['id' => $eventtopic->id])}}" id="form" method="post">
          @csrf
          @method('DELETE')
    			<div>
    				<label for="Name">Naam</label>
    				<input type="text" readonly id="Name" name="name" value="{{$eventtopic->name}}">
    			</div>
        </form>
        <a href={{route('eventtopics.index')}}>index</a>
      </article>
    </main>
  </body>
</html>
