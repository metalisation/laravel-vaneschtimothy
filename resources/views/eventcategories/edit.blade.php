<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="Timothy Van Esch">
        <link rel="stylesheet" href="{{ asset('css/fricfrac.css') }}" type="text/css"/>
        <title>Fric-Frac</title>
    </head>
    <body>
        <main>
            <article>
              <header>
                  <h2>EventCategory</h2>
              <nav>
                 <a href="{{route('eventcategories.index')}}">Annuleren</a>
              </nav>
              </header>
              <form method="post" action="{{ route('eventcategories.update', $eventcategory->id) }}">
                  @method('PATCH')
                  @csrf
                  <div>
                    <label for="name">Name:</label>
                    <input type="text"  name="name" value="{{ $eventcategory->name }}" />
                  </div>
                  
                  <button type="submit">Update</button>
              </form>
            </article>
            @include('eventcategories.readingAll')
        </main>
      </body>
    </html>