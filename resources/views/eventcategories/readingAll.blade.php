<aside>
    <table>
        <thead>
            <tr>
                <td>select</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($eventcategoryList as $eventcategory) 
            <tr>
                <td><a href="{{ route('eventcategories.show',$eventcategory->id)}}">show</a></td>
                <td>{{$eventcategory->name}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</aside>