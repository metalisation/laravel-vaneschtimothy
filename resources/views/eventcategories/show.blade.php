<?php //edit = updateone ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="Timothy Van Esch">
        <link rel="stylesheet" href="{{ asset('css/fricfrac.css') }}" type="text/css"/>
        <title>Fric-Frac</title>
    </head>
    <body>
        <main>
          <article>
            <header>
                    <h2>EventCategory</h2>
                    <nav>
                        <a href="{{route('eventcategories.edit', ['id' => $eventcategory->id])}}">Edit</a>
                        <a href="{{route('eventcategories.create')}}">Create</a>
                        <button type="submit" value="delete" form="form">Delete</button>
                        <a href="{{route('eventcategories.index')}}">Annuleren</a>
                    </nav>
                </header>
            <form action="{{ route('eventcategories.destroy', ['id' => $eventcategory->id])}}" id="form" method="post">
              @csrf
              @method('DELETE')
                    <div>
                        <label for="Name">Naam</label>
                        <input type="text" readonly id="Name" name="name" value="{{$eventcategory->name}}">
                    </div>
            </form>
            <a href={{route('eventcategories.index')}}>index</a>
          </article>
        </main>
      </body>
    </html>