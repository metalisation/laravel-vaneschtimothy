<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('location');
            $table->datetime('starts')->nullable();
            $table->datetime('ends')->nullable();
            $table->string('image');
            $table->string('description');
            $table->string('organisername');
            $table->string('organiserdescription');
            $table->unsignedInteger('eventcategoryid');
            $table->unsignedInteger('eventtopicid');
            $table->timestamps();

            $table->foreign('eventcategoryid')->references('id')->on('eventcategory');
            $table->foreign('eventtopicid')->references('id')->on('eventtopic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
