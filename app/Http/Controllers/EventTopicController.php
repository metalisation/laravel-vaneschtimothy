<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventTopic;

class EventTopicController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $eventtopics = EventTopic::all();
        return view('eventtopics.index', array('eventtopicsList' =>$eventtopics));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $eventtopics = EventTopic::all();
        return view('eventtopics.create', array('eventtopicsList' =>$eventtopics));
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
        ]);
        $eventtopics = new EventTopic;
        $eventtopics->name = $request->name;
        $eventtopics->save();
        return redirect('/eventtopics')->with('success', 'eventtopic has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $eventtopics = EventTopic::find($id);
        $eventtopicsList = EventTopic::all();
        
        return view('eventtopics.show', array('eventtopic' => $eventtopics));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $eventtopics = EventTopic::find($id);
        $eventtopicsList = EventTopic::all();
        return view('eventtopics.edit', compact('eventtopics', 'eventtopicsList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);

        $eventtopics = EventTopic::find($id);
        $eventtopics->name = $request->get('name');
     
        $eventtopics->save();

        return redirect('eventtopics')->with('success', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $eventtopics = EventTopic::find($id);
        $eventtopics->delete();
        
        return redirect('eventtopics');
    }
}
