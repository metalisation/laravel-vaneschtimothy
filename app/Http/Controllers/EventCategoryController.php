<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EventCategory;

class EventCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventcategories = EventCategory::all();
        return view('eventcategories.index', array('eventcategoryList' =>$eventcategories));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eventcategories = EventCategory::all();
        return view('eventcategories.create', array('eventcategoryList' =>$eventcategories));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
        ]);
        $eventcategory = new EventCategory;
        $eventcategory->name = $request->name;
        $eventcategory->save();
        return redirect('/eventcategories')->with('success', 'eventcategory has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eventcategory = EventCategory::find($id);
        $eventcategoryList = EventCategory::all();
        
        return view('eventcategories.show', array('eventcategory' => $eventcategory));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventcategory = EventCategory::find($id);
        $eventcategoryList = EventCategory::all();
        return view('eventcategories.edit', compact('eventcategory', 'eventcategoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);

        $eventcategory = EventCategory::find($id);
        $eventcategory->name = $request->get('name');
     
        $eventcategory->save();

        return redirect('eventcategories')->with('success', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventcategory = EventCategory::find($id);
        $eventcategory->delete();
        
        return redirect('eventcategories');
    }
}
